package noobbot.math;

import com.google.common.collect.ImmutableMap;
import noobbot.physics.*;
import org.junit.Test;

/**
 * Created by anita on 4/28/14.
 */
public class GamePhysicsTest {

    @Test
    public void test() {
        /*
            Speed Model Training: A=0.248019, Fs=0.029353 (mse=0.031029)
Drift Model TR=1.843330, K=-0.463849, C=0.166001
         */

        GamePhysics gamePhysics = new GamePhysics( new GamePhysicsMap(
                new SpeedModel(
                        new SpeedModel.ModelData(0.178668, 0.017118,0.0)),
                new DriftModel(
                        new DriftModel.ModelData(0.849874,
                                2.121189,
                                0.104308,
                                0.0))),
                ImmutableMap.<Integer, GamePhysicsMap>of(),
                new CurveVelocityModel2());

        /*
          ACTUAL 0, 5, 474.375, 8.309, 40.208, 0.000, 6.808, 0.355, 0.035

             FINAL: 959.799, 0.000, 90.000, 0.000, -Infinity, -2.080, -0.142, 179769313486231570000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000.000

         */
        //        TIME ESTIMATE: 8.013356379489153, 0.0, 0.9601331319722703

        double entryVelocity = 8.013356379489153;
        double throttle = 0.0;
        double positionTarget = 0.9601331319722703;

        double timeEstimate = gamePhysics.getDefaultPhysics()
                .findTimeNew(entryVelocity, throttle, positionTarget);
        double exitVelocity = gamePhysics.getDefaultPhysics()
                .findVelocity(entryVelocity, throttle, timeEstimate);

        System.out.println(timeEstimate);
        System.out.println(exitVelocity);


    }
}
