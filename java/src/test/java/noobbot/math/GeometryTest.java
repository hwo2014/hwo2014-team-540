package noobbot.math;

import noobbot.msg.Orientation;
import noobbot.msg.Position;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by anita on 4/20/14.
 */
public class GeometryTest {

    @Test
    public void test() {
        Geometry geometry = new Geometry(new Orientation(new Position(0, 0), 0.0));

        geometry.move(10);
        System.out.println(geometry);
        geometry.turnLeft(90, 5);
        System.out.println(geometry);
        geometry.turnRight(45, 3);
        System.out.println(geometry);
        geometry.turnRight(45, 3);
        System.out.println(geometry);

        geometry.turnRight(30, 2);
        System.out.println(geometry);
        geometry.turnRight(30, 2);
        System.out.println(geometry);
        geometry.turnRight(30, 2);
        System.out.println(geometry);

        geometry.move(6);
        System.out.println(geometry);

        geometry.turnRight(40, 4);
        System.out.println(geometry);
        geometry.turnRight(40, 4);
        System.out.println(geometry);
        geometry.turnRight(10, 4);
        System.out.println(geometry);

        Orientation orientation = geometry.toOrientation();
        assertEquals(16, orientation.getPosition().getX(),0.001);
        assertEquals(-4, orientation.getPosition().getY(),0.001);
        assertEquals(-1, Math.cos(orientation.getAngle() * Math.PI / 180),0.001);
        assertEquals(0, Math.sin(orientation.getAngle() * Math.PI / 180),0.001);
    }
}
