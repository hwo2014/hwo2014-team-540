package noobbot.msg;

import lombok.Data;

import java.util.List;

/**
 */
@Data
public class Race {

    private final Track track;

    private final List<Car> cars;

    private final RaceSession raceSession;

}
