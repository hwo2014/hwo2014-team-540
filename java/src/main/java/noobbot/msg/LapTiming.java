package noobbot.msg;

import lombok.Data;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class LapTiming {

    private final Integer lap;

    private final Integer ticks;

    private final Integer millis;
}
