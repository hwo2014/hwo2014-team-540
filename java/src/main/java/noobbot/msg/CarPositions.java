package noobbot.msg;

import lombok.Data;

import java.util.List;

/**
 */
@Data
public class CarPositions {

    private final List<CarPosition> carPositionList;

    public CarPosition findMatchingCar(CarIdentity carId) {
        String color = carId.getColor();
        for(CarPosition cp: carPositionList) {
            if (cp.getIdentity().getColor().equals(color)) {
                return cp;
            }
        }
        return null;
    }
}
