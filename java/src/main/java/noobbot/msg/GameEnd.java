package noobbot.msg;

import lombok.Data;

import java.util.List;

/**
 * Created by anita on 4/17/14.
 */
@Data
public class GameEnd {

    private final List<RaceResult> results;
    private final List<BestLap> bestLaps;
}
