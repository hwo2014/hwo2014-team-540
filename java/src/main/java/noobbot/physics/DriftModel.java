package noobbot.physics;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.Builder;
import noobbot.math.LinearRegression;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

import java.util.List;

/**
 * http://mathb.in/15855?key=b86fdd64bf5d60bba30050bafdeca7dd56bc32b3
 *
 */
@RequiredArgsConstructor
@ToString
public class DriftModel {

    private final ModelData modelData;

    /**
     */
    @Data
    public static class ModelData {

        // T
        private final double thrustFactor;

        // K (for the relaxation length)
        private final double springFactor;

        // C
        private final double dampingFactor;

        // mu_s (sliding friction coefficient for drift)
        private final double slidingCoefficient;
    }


    /**
     */
    @Getter @Builder (fluent = false)
    public static class Input {

        private final double velocity;

        private final double acceleration;

        private final double curvature;

        private final double driftDegrees;

        private final double driftDegreesRate;
    }

    @Data
    public static class Output {

        private final double driftDegreesAccel;
    }

    public static RealVector newInputVector(Input measure) {
        RealVector realVector = new ArrayRealVector(3);
        realVector.setEntry(0, 1);
        realVector.setEntry(1, 1);
        realVector.setEntry(2, 1);
        return realVector;
    }

    public static ModelData train(List<Input> input, List<Output> output) {
        if (input.size() != output.size()) {
            throw new IllegalArgumentException("input and output size must be equal");
        }
        int n = input.size();
        RealMatrix parameters = new Array2DRowRealMatrix(n, 3);
        for (int i = 0; i < n; i++) {
            Input measure = input.get(i);
            parameters.setRowVector(i, newInputVector(measure));
        }

        RealVector outcome = new ArrayRealVector(n);
        for (int i = 0; i < n; i++) {
            double da = output.get(i).getDriftDegreesAccel();
            outcome.setEntry(i, da);
        }

        LinearRegression.RegressionResult result = LinearRegression.solve(parameters, outcome);

        RealVector solution = result.getSolution();
        System.out.printf("Drift Model TR=%f, K=%f, C=%f%n, LS=%f (mse=%f)\n",
                solution.getEntry(0),
                solution.getEntry(1),
                solution.getEntry(2),
                0.0,
                result.getMeanSquaredError());
        return new ModelData(solution.getEntry(0), solution.getEntry(1), solution.getEntry(2), 0.0);
    }


    public Output process(Input input) {
        RealVector parameters = newInputVector(input);

        RealVector solution = new ArrayRealVector(3);
        solution.setEntry(0, modelData.getThrustFactor());
        solution.setEntry(1, modelData.getSpringFactor());
        solution.setEntry(2, modelData.getDampingFactor());
//        solution.setEntry(3, modelData.getSlidingCoefficient());

        double driftDegreesAccel = solution.dotProduct(parameters);

        return new Output(driftDegreesAccel);
    }

}
