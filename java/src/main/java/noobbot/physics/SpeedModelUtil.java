package noobbot.physics;

import noobbot.data.FullMetric;
import noobbot.data.ThrottleMetric;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/22/14.
 */
public class SpeedModelUtil {

    public static SpeedModel.Input makeInput(FullMetric fullMetric) {
        ThrottleMetric throttleMetric = fullMetric.getThrottleMetric();
        double turboFactor = 1.0;
        if (throttleMetric.getTurboAvailable() != null) {
            turboFactor = throttleMetric.getTurboAvailable().getTurboFactor();
        }
        double throttle = throttleMetric.getThrottle() * turboFactor;

        return new SpeedModel.Input(throttle, fullMetric.getPositionMetric().getVelocity());
    }

    public static List<SpeedModel.Input> prepareInputList(List<FullMetric> fullMetricList) {
        List<SpeedModel.Input> inputList = new ArrayList<>();
        // Skip one entry because that is not useful because we don't know the corresponding driftAccel.
        for (int i = 0; i + 1 < fullMetricList.size(); i++) {
            inputList.add(makeInput(fullMetricList.get(i)));
        }
        return inputList;
    }

    public static List<SpeedModel.Output> prepareOutputList(List<FullMetric> fullMetricList) {
        List<SpeedModel.Output> outputList = new ArrayList<>();
        for (int i = 0; i + 1 < fullMetricList.size(); i++) {
            // Get the next one because it is predicting the next value.
            outputList.add(new SpeedModel.Output(fullMetricList.get(i + 1).getPositionMetric().getAcceleration()));
        }
        return outputList;
    }

    public static SpeedModel train(List<FullMetric> fullMetricList) {
        return new SpeedModel(SpeedModel.train(prepareInputList(fullMetricList),
                prepareOutputList(fullMetricList)));
    }

}
