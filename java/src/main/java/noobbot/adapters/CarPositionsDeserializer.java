package noobbot.adapters;

import com.google.gson.*;
import lombok.RequiredArgsConstructor;
import noobbot.msg.CarPosition;
import noobbot.msg.CarPositions;

import javax.inject.Provider;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anita on 4/17/14.
 */
@RequiredArgsConstructor
public class CarPositionsDeserializer implements JsonDeserializer<CarPositions> {

    private final Provider<Gson> gsonProvider;

    @Override
    public CarPositions deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<CarPosition> carPositionList = new ArrayList<>();

        JsonArray jsonArray = json.getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            CarPosition carPosition = gsonProvider.get().fromJson(jsonArray.get(i), CarPosition.class);
            carPositionList.add(carPosition);
        }

        return new CarPositions(carPositionList);
    }
}
