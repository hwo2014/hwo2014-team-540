package noobbot.adaptive;

import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import noobbot.controller.PhysicsStore;
import noobbot.controller.PlanStore;
import noobbot.controller.SolvingThread;
import noobbot.data.*;
import noobbot.msg.GameInit;
import noobbot.msg.PiecePosition;
import noobbot.msg.Track;
import noobbot.msg.TrackPiece;
import noobbot.physics.DriftModel;
import noobbot.physics.GamePhysicsMap;
import noobbot.physics.SolvingParams;

import javax.inject.Named;
import java.util.List;

/**
 * Created by anita on 4/26/14.
 */
public class StandardAdaptive implements Adaptive {

    private final GameInit gameInit;

    private final Track track;

    private final PlanStore planStore;

    private final PhysicsStore physicsStore;

    private final double defaultThrottle;

    private final SolvingThread solvingThread;

//    private boolean inBraking = false;
    private double MAX_DRIFT = 60;

    @Inject
    public StandardAdaptive(GameInit gameInit,
                            PlanStore planStore,
                            PhysicsStore physicsStore,
                            @Named("defaultThrottle") double defaultThrottle, SolvingThread solvingThread) {
        this.gameInit = gameInit;
        this.planStore = planStore;
        this.physicsStore = physicsStore;
        this.solvingThread = solvingThread;
        this.track = gameInit.getRace().getTrack();
        this.defaultThrottle = defaultThrottle;
        System.out.println("Adapt - Lap, PieceIndex, InPiecePosition, Radius, drift, drift rate, drift accel, Expected Velocity, Actual Velocity, Throttle");
    }

    @Override
    public CarControl getCarControl(SolvingParams solvingParams) {

        //System.out.println(solvingParams);
        FullMetric entryFullMetric = solvingParams.getFullMetric();
        PiecePosition entryPiecePosition = solvingParams.getPiecePosition();

        PredictivePlan predictivePlan = planStore.getPlan();
        GamePhysicsMap gamePhysics = physicsStore.get().getGamePhysicsMap(
                solvingParams.getFullMetric().getStateSpaceIndex());

//        System.out.println("Plan Size: " + predictivePlan.getFullMetricList().size());

//        double maxFirstTickDistance = gamePhysics.maxSpeed();
        if (solvingParams.getFiredNext() == null) {
            int nextLap;
            int nextPiece;
            int currentPiece = entryPiecePosition.getPieceIndex();
            int currentLap = entryPiecePosition.getLap();
            if (currentPiece == track.getPieces().size()) {
                nextPiece = 0;
                nextLap = currentLap + 1;
            } else {
                nextPiece = currentPiece + 1;
                nextLap = currentLap;
            }

            int pieceId = nextLap * track.getPieces().size() + nextPiece;
            Integer laneTarget = predictivePlan.getLaneTarget().get(pieceId);
            if (laneTarget != null) {
                if (laneTarget < entryPiecePosition.getLane().getEndIndex()) {
                    return CarControl.switchLeft();
                } else if (laneTarget > entryPiecePosition.getLane().getEndIndex()) {
                    return CarControl.switchRight();
                }
            } else {
                System.out.println(predictivePlan.getLaneTarget());
            }
        }

        double turboFactor = 1.0;
        if (entryFullMetric.getThrottleMetric().getTurboAvailable() == null) {
            // No turbo is active right now, so ..
            Preconditions.checkNotNull(predictivePlan.getTurboPlanList());
            for (TurboPlan turboPlan : predictivePlan.getTurboPlanList()) {
                if (turboPlan.getPosition().getLap() == entryPiecePosition.getLap() &&
                        turboPlan.getPosition().getPieceIndex() == entryPiecePosition.getPieceIndex() &&
                        turboPlan.getPosition().getInPieceDistance() < entryPiecePosition.getInPieceDistance()) {
                    return CarControl.applyTurbo();
                }
            }
        } else {
            //turboFactor = entryFullMetric.getThrottleMetric().getTurboAvailable().getTurboFactor();
        }

        // FIXME FIXME FIXME (ANITA): Respect turboFactor only to time given. Assumption for now is
        // plan is at "almost" tick level, turbo factor is at least constantin that length.

        PositionMetric entryPositionMetric = entryFullMetric.getPositionMetric();
        double entryPosition = entryPositionMetric.getPosition();

        List<FullMetric> fullMetricPlan = predictivePlan.getFullMetricList();
        int searchPos = Integer.MAX_VALUE;

        if (fullMetricPlan != null) {
            for (int i = 0; i < fullMetricPlan.size(); i++){
                if (fullMetricPlan.get(i).getPositionMetric().getPosition() > entryPosition) {
                    searchPos = i;
                    break;
                }
            }
        }

        if (searchPos == Integer.MAX_VALUE) {
            // Fallback to safe plan.
            if (Math.abs(entryFullMetric.getDriftMetric().getDriftDegrees()) > 30) {
                return CarControl.applyThrottle(0.0);
            } else {
                return CarControl.applyThrottle(defaultThrottle);
            }
        }


        FullMetric fullMetricTarget = fullMetricPlan.get(searchPos);

        PositionMetric positionMetricTarget = fullMetricTarget.getPositionMetric();

        double entryVelocity = entryPositionMetric.getVelocity();

        double positionTarget = positionMetricTarget.getPosition();
        double velocityTarget = positionMetricTarget.getVelocity();

        System.out.println("entryPosition: " + entryPosition + ", positionTarget: " + positionTarget);
        double distance = positionTarget - entryPosition;

        double throttle;
        double radius = gameInit.getRace().getTrack().getPieces().get(entryPiecePosition.getPieceIndex())
                .getComputedLaneRadius(entryPiecePosition.getLane().getEndIndex());

        if (entryVelocity < velocityTarget) {
            if (distance - entryVelocity * 2 > 0) {
                // We have one more tick available.
                System.out.println("There is at least two more ticks, go with 1.0");
                throttle = 1.0;
            } else {
                // Stay at current speed.
                System.out.println("Stay at current speed");
                throttle = gamePhysics.findQuickThrottle(0, entryVelocity) / turboFactor;
            }
        } else {
            // We need to decelerate.
            double minDistanceToBrake = gamePhysics.findMinBrakingDistance(entryVelocity, velocityTarget);
            double aspirationalJump = Math.max(distance - entryVelocity * 3, 0);
            System.out.println("minDistanceToBrake: " + minDistanceToBrake + ", distance: " + distance + ", " +
                    "aspirationalJump: " + aspirationalJump);
            if (aspirationalJump <= minDistanceToBrake) {
                // Stay at current speed.
                System.out.println("Brake now!!");
                throttle = 0.0;
            } else {
                System.out.println("Stay at current speed");
                throttle = gamePhysics.findQuickThrottle(0, entryVelocity) / turboFactor;
            }
        }

        double drift = entryFullMetric.getDriftMetric().getDriftDegrees();
        double driftRate = entryFullMetric.getDriftMetric().getDriftRateDegrees();
        double driftAccel = entryFullMetric.getDriftMetric().getDriftAccelDegrees();

        if ((Math.abs(drift) > 30) &&
                (driftRate * driftAccel > 0)){
            System.out.println("Drift Acceleration and rate in same direction - Brake now");
            //physicsStore.get().getCurveVelocityModel().updateDriftHint(entryPiecePosition.getPieceIndex(), entryPiecePosition.getLane().getEndIndex());
            throttle = 0.0;
        }

        if (Math.abs(drift) > MAX_DRIFT) {
            System.out.println("Too much drift - Brake now");
            //physicsStore.get().getCurveVelocityModel().updateDriftHint(entryPiecePosition.getPieceIndex(), entryPiecePosition.getLane().getEndIndex());
            throttle = 0.0;
        }


        if ((MAX_DRIFT - Math.abs(drift))/Math.abs(driftRate) < 15) {
            System.out.println("Crash in 15 - Brake now");
            //physicsStore.get().getCurveVelocityModel().updateDriftHint(entryPiecePosition.getPieceIndex(), entryPiecePosition.getLane().getEndIndex());
            throttle = 0.0;
        }

        // Second order approximation
        // s = ut + 1/2 at^2
        // max s happens:
        // v = u + at = 0
        // t = u/a
        if (Math.abs(driftAccel) > 0.01) {
            double endTime = driftRate / driftAccel;

            double maximaDrift;
            if (endTime > 0 && endTime < 15) {
                maximaDrift = drift + driftRate * endTime  + .5 * driftAccel * endTime * endTime;
                if (Math.abs(maximaDrift) > MAX_DRIFT) {
                    System.out.println("Crash predicted in " + endTime);
                    throttle = 0.0;
                }
            }
        }


        System.out.printf("Adapt - %d, %d, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f%n",
                entryPiecePosition.getLap(), entryPiecePosition.getPieceIndex(),
                entryPiecePosition.getInPieceDistance(), radius,
                entryFullMetric.getDriftMetric().getDriftDegrees(),
                entryFullMetric.getDriftMetric().getDriftRateDegrees(),
                entryFullMetric.getDriftMetric().getDriftAccelDegrees(),
                velocityTarget, entryVelocity,
                throttle);

        return CarControl.applyThrottle(throttle);


    }
}
