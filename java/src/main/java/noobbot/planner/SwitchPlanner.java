package noobbot.planner;

import com.google.inject.ImplementedBy;
import noobbot.data.SwitchLane;
import noobbot.msg.PiecePosition;

import java.util.Map;

/**
 * Created by anita on 4/25/14.
 */
@ImplementedBy(ShortestSwitchPlanner.class)
public interface SwitchPlanner {

    /**
     * index = lap * totalPieces * totalLanes +
     *         piece * totalLanes +
     *         laneIndex
     *
     * Map&lt; index, SwitchLane &gt; The plan to apply "before"
     * entering "index" position.
     *
     * @type firedNext if a switch plan that is already fired is
     *   outstanding. Then there is no choice but to set this in
     *   the next turn that is available. This is nullable to
     *   indicate no outstanding commands are made.
     *
     * @return
     */
    Map<Integer, SwitchLane> switchPlan(PiecePosition piecePosition,
                                        SwitchLane firedNext);
}
