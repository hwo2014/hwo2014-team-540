package noobbot.planner;

import com.google.common.base.Preconditions;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import lombok.Setter;
import noobbot.controller.PhysicsStore;
import noobbot.data.PredictivePlan;
import noobbot.data.SwitchLane;
import noobbot.data.TurboPlan;
import noobbot.msg.GameInit;
import noobbot.physics.SolvingException;
import noobbot.physics.SolvingParams;

import java.util.List;
import java.util.Map;

/**
 * Created by anita on 4/25/14.
 */
public class StandardPredictive implements Predictive {

    private final GameInit gameInit;

    private final PhysicsStore physicsStore;

    private final SwitchPlanner switchPlanner;

    private final TurboPlanner turboPlanner;

    @Setter
    private volatile Integer maxDrift;

    @Inject
    public StandardPredictive(GameInit gameInit, PhysicsStore physicsStore, SwitchPlanner switchPlanner,
                              TurboPlanner turboPlanner, @Named("maxDrift") Integer maxDrift) {
        this.gameInit = gameInit;
        this.physicsStore = physicsStore;
        this.switchPlanner = switchPlanner;
        this.turboPlanner = turboPlanner;
        this.maxDrift = maxDrift;
    }

    @Override
    public PredictivePlan newPlan(SolvingParams solvingParams, int depth)
            throws SolvingException, InterruptedException {

        // TODO(anita): Pass the next fired.
        Map<Integer, SwitchLane> switchLaneMap =
                switchPlanner.switchPlan(solvingParams.getPiecePosition(),
                solvingParams.getFiredNext());

        Preconditions.checkNotNull(switchLaneMap);
        List<TurboPlan> turboPlanList = turboPlanner.turboPlan();
        Preconditions.checkNotNull(turboPlanList);

        StandardPredictiveInstance standardPredictiveInstance =
                new StandardPredictiveInstance(
                gameInit,
                physicsStore.get(),
                turboPlanList,
                maxDrift,
                switchLaneMap);

        return standardPredictiveInstance.getPredictivePlan(solvingParams, depth);
    }
}
