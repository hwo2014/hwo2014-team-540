package noobbot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import noobbot.adapters.CarPositionsDeserializer;
import noobbot.adapters.CarPositionsSerializer;
import noobbot.adapters.MsgWrapperDeserializer;
import noobbot.adapters.MsgWrapperSerializer;
import noobbot.base.MsgWrapper;
import noobbot.msg.CarPositions;
import noobbot.wire.BotEngine;
import noobbot.wire.BotListener;
import noobbot.wire.BotReader;
import noobbot.wire.BotWriter;

import javax.inject.Provider;
import java.io.BufferedReader;
import java.io.PrintWriter;

/**
 * Created by anita on 4/17/14.
 */
public class NoobBotModule extends AbstractModule {

    private final BufferedReader reader;

    private final PrintWriter writer;

    public NoobBotModule(BufferedReader reader, PrintWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    @Override
    protected void configure() {
        bind(BotListener.class).to(NoobBot.class);

    }

    @Provides @Singleton
    public Gson providesGson(GsonBuilder gsonBuilder) {
        return gsonBuilder.create();
    }

    @Provides @Singleton
    public CarPositionsSerializer providesCarPositionsSerializer(Provider<Gson> gsonProvider) {
        return new CarPositionsSerializer(gsonProvider);
    }

    @Provides @Singleton
    public CarPositionsDeserializer providesCarPositionsDeserializer(Provider<Gson> gsonProvider) {
        return new CarPositionsDeserializer(gsonProvider);
    }

    @Provides @Singleton
    public MsgWrapperDeserializer providesMsgWrapperDeserializer(Provider<Gson> gsonProvider) {
        return new MsgWrapperDeserializer(gsonProvider);
    }

    @Provides @Singleton
    public MsgWrapperSerializer providesMsgWrapperSerializer(Provider<Gson> gsonProvider) {
        return new MsgWrapperSerializer(gsonProvider);
    }

    @Provides @Singleton
    public GsonBuilder providesGsonBuilder(MsgWrapperDeserializer msgWrapperDeserializer,
                                           MsgWrapperSerializer msgWrapperSerializer,
                                           CarPositionsDeserializer carPositionsDeserializer,
                                           CarPositionsSerializer carPositionsSerializer) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(MsgWrapper.class, msgWrapperSerializer);
        gsonBuilder.registerTypeAdapter(MsgWrapper.class, msgWrapperDeserializer);
        gsonBuilder.registerTypeAdapter(CarPositions.class, carPositionsDeserializer);
        gsonBuilder.registerTypeAdapter(CarPositions.class, carPositionsSerializer);
        return gsonBuilder;
    }

    @Provides @Singleton
    public BotReader providesBotReader(Gson gson) {
        return new BotReader(gson, reader);
    }

    @Provides @Singleton
    public BotWriter providesBotWriter(Gson gson) {
        return new BotWriter(gson, writer);
    }

    @Provides @Singleton
    public NoobBot providesNoobBot(BotWriter botWriter) {
        return new NoobBot(botWriter);
    }

    @Provides @Singleton
    public BotEngine providesBotEngine(BotListener botListener,
                                       BotReader botReader) {
        return new BotEngine(botListener, botReader);
    }
}
