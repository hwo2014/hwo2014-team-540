package noobbot.controller;

import com.google.inject.Singleton;
import noobbot.physics.GamePhysics;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by anita on 4/25/14.
 */
@Singleton
public class PhysicsStore {

    private final AtomicReference<GamePhysics> gamePhysics =
            new AtomicReference<>();

    public GamePhysics get() {
        return gamePhysics.get();
    }

    public void set(GamePhysics gamePhysics) {
        this.gamePhysics.set(gamePhysics);
    }

}
