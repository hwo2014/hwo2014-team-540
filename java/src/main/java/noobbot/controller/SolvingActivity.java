package noobbot.controller;

import com.google.inject.Inject;
import lombok.Setter;
import noobbot.data.PredictivePlan;
import noobbot.physics.SolvingException;
import noobbot.physics.SolvingParams;
import noobbot.planner.Predictive;

/**
 * Created by anita on 4/25/14.
 */
@Setter
public class SolvingActivity {

    private final PlanStore planStore;

    private final Predictive predictive;

    @Inject
    public SolvingActivity(PlanStore planStore,
                           Predictive predictive) {
        this.planStore = planStore;
        this.predictive = predictive;
    }

    public Runnable newSolver(final SolvingParams solvingParams,
                              final int depth) {
        return new Runnable() {

            @Override
            public void run() {
                PredictivePlan plan = null;
                try {
                    plan = predictive.newPlan(solvingParams, depth);
                    planStore.setPlan(plan);
                } catch (SolvingException | InterruptedException e) {
                    throw new RuntimeException(e);
                }

            }
        };
    }
}

