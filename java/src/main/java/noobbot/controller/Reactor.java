package noobbot.controller;

import com.google.inject.Inject;
import lombok.Getter;
import noobbot.adaptive.Adaptive;
import noobbot.data.CarControl;
import noobbot.physics.SolvingParams;
import noobbot.strategy.Strategy;
import noobbot.trainer.PhysicsTrainer;

import java.util.concurrent.ExecutionException;

/**
 * Created by anita on 4/25/14.
 */
public class Reactor {

    private final SolvingThread solvingThread;

    private final PhysicsTrainer physicsTrainer;

    private final Strategy strategy;

    private final Adaptive adaptive;

    @Getter
    private boolean isDataCollected = true;

    @Inject
    public Reactor(SolvingThread solvingThread,
                   PhysicsTrainer physicsTrainer, Strategy strategy,
                   Adaptive adaptive) {
        this.solvingThread = solvingThread;
        this.physicsTrainer = physicsTrainer;
        this.strategy = strategy;
        this.adaptive = adaptive;
    }

    public CarControl getAction(SolvingParams solvingParams, boolean preStartPosition) {
        solvingThread.setSolvingParams(solvingParams);

        if (preStartPosition) {
            try {
                solvingThread.blockingSolve();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        } else {
            try {
                solvingThread.blockingSolve();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        physicsTrainer.recordMetric(solvingParams.getFullMetric());

        CarControl carControl = adaptive.getCarControl(solvingParams);

        carControl = strategy.takeAction(carControl);

        return carControl;
    }

    public void shutdown() {
        solvingThread.shutdown();
    }

    public void updateCrash() {
        physicsTrainer.disableDataCollection();
        physicsTrainer.train();
        solvingThread.startUrgent();
        isDataCollected = false;
    }

    public void updateSpawn() {
        physicsTrainer.enableDataCollection();
        isDataCollected = true;
    }

    public void stopDataCollection() {
        physicsTrainer.disableDataCollection();
        isDataCollected = false;
    }

    public void startDataCollection() {
        physicsTrainer.enableDataCollection();
        isDataCollected = true;
    }

    public void setSpeedTrained() {
        physicsTrainer.setSpeedTrained();
    }
}
