package noobbot.data;

import lombok.Data;
import noobbot.controller.Reactor;
import noobbot.msg.*;
//import noobbot.physics.PhysicsAtlasUtil;
import noobbot.physics.SolvingParams;

/**
 * Created by vaidhy on 20/4/14.
 */
@Data
public class CarInfo {

    GameInit gameData;
    CarIdentity myCarIdentity;
    int prevTick = -1;
    int currTick = 0;

    double prevDistance = 0;
    double prevVelocity = 0;
    double prevAngle;

    double totalTrackPiecesCovered = 0.0;
    int prevTrackIndex, currTrackIndex = 0;

    private TrackPiece currTrack;
    private double totalDistanceCovered = 0.0;
    private double velocity = 0;
    private double accel = 0;
    private double angleRate = 0;

    private int lap;
    private int pieceIndex;
    private double angle;

    private double throttle;

    private int laneIndex;

    private int endLaneIndex;

    private double inPieceDistance;

    private double angleAccel;
    private double prevAngleRate;
    private double signedCurvature;

    private PiecePosition currPiecePosition;

    private SwitchLane pendingOrder = null;

    private int pieceOfIssuingSwitchingOrder;

    private TurboAvailable turboApplied = null;

    private int turboStartTime = 0;

    private int totalLanes = 0;

    public void setGameData(GameInit gameInit) {
        this.gameData = gameInit;
        currTrack = gameData.getRace().getTrack().getPieces().get(currTrackIndex);
        totalLanes =gameData.getRace().getTrack().getLanes().size();
    }

    int getPieceId() {
        return lap * gameData.getRace().getTrack().getPieces().size()
                + pieceIndex;
    }

    public void updateSwitchOrder(SwitchLane switchLane) {
        this.pendingOrder = switchLane;
        this.pieceOfIssuingSwitchingOrder = getPieceId();
    }

    public void updatePosition(CarPosition myCar, int gameTick) {
        currPiecePosition = myCar.getPiecePosition();
        laneIndex = currPiecePosition.getLane().getStartIndex();
        endLaneIndex = currPiecePosition.getLane().getEndIndex();
        currTrackIndex = currPiecePosition.getPieceIndex();

        if (pendingOrder != null &&
                getPieceId() != pieceOfIssuingSwitchingOrder) {
            if (laneIndex != endLaneIndex) {
                pendingOrder = null;
            }
        }

        // Check to see if computed lengths and actual lengths match
        if (myCar.getPiecePosition().getInPieceDistance() > currTrack.getComputedLaneLength(laneIndex)) {
            System.out.printf(
                    "Computed Distance and actual distance error. Track : %s, InPieceLength: %.3f, " +
                            "ComputedLength: %.3f%n",
                    currTrack, currPiecePosition.getInPieceDistance(), currTrack.getComputedLaneLength(laneIndex)
            );
        }

        if (prevTrackIndex != currTrackIndex) {
            if (prevTick != -1) {
                totalTrackPiecesCovered += gameData.getRace().getTrack().getPieces().get(prevTrackIndex).getComputedLaneLength(laneIndex);
            }
            prevTrackIndex = currTrackIndex;
            currTrack = gameData.getRace().getTrack().getPieces().get(currTrackIndex);
        }

        lap = currPiecePosition.getLap();
        pieceIndex = currPiecePosition.getPieceIndex();
        angle = myCar.getAngle();
        currTick = gameTick;

        inPieceDistance = currPiecePosition.getInPieceDistance();
        totalDistanceCovered = totalTrackPiecesCovered + inPieceDistance;

        double dx = (totalDistanceCovered - prevDistance);
        double dt = (gameTick - prevTick);

        // Ignore start
        if (prevTick != -1) {
            // Linear acceleration approximation within tick
            velocity = dx / dt;
            accel = (velocity - prevVelocity)/ dt;
            double dp = (angle - prevAngle);

            // Linear acceleration approximation within tick
            angleRate = dp / dt;
            angleAccel = (angleRate - prevAngleRate) / (gameTick - prevTick);
        }
        signedCurvature = currTrack.getComputedCurvature(laneIndex);
    }


    public void log() {
        //System.out.printf("%d, %.3f, %d, %d, %d, %.3f, %.3f, %.3f\n",
        //        currTick, totalDistanceCovered, lap, currTrackIndex, currTrack.getDirection(),
        //        currTrack.getComputedCurvature(laneIndex), angle, throttle);
        System.out.printf("ACTUAL %d, %d, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f\n",
                lap, currTrackIndex, totalDistanceCovered, velocity, angle, throttle,
                angleRate, angleAccel, accel);
    }

    public FullMetric estimate() {
        PositionMetric positionMetric = new PositionMetric(totalDistanceCovered, velocity, accel);
        DriftMetric driftMetric = new DriftMetric(angle, angleRate, angleAccel);
        ThrottleMetric throttleMetric = new ThrottleMetric(throttle, turboApplied, turboStartTime);

        //int stateSpaceIndex = PhysicsAtlasUtil.getStateSpaceIndex(pieceIndex, totalLanes, laneIndex);
        return new FullMetric(positionMetric, driftMetric, throttleMetric, signedCurvature, 0);
    }

    public void finishUpdate() {
        prevDistance = totalDistanceCovered;
        prevTick = currTick;
        prevVelocity = velocity;
        prevAngle = angle;
        prevAngleRate = angleRate;
    }

    public void updateCrash() {
        this.velocity = 0;
        this.angle = 0;
        this.angleRate = 0;

        this.prevAngle = 0;
        this.prevAngleRate = 0;
        this.prevVelocity = 0;
    }

    public CarControl getCarControl(Reactor reactor, boolean preStartPosition) {
        if (reactor != null) {
            PiecePosition piecePosition = PiecePosition.builder()
                    .lap(lap)
                    .pieceIndex(pieceIndex)
                    .lane(new CarPosition.LanePosition(laneIndex, endLaneIndex))
                    .inPieceDistance(inPieceDistance)
                    .build();

            SolvingParams solvingParams = SolvingParams.builder()
                    .piecePosition(piecePosition)
                    .fullMetric(estimate())
                    .gameTick(getCurrTick())
                    .firedNext(pendingOrder)
                    .build();
            CarControl carControl = reactor.getAction(solvingParams, preStartPosition);
            if (carControl != null && carControl.getAction() == Action.THROTTLE) {
                throttle = carControl.getThrottle();
            }
            return carControl;
        } else {
            return CarControl.applyThrottle(.3);
        }
    }

    public void startTurbo(TurboAvailable turbo) {
        this.turboApplied = turbo;
    }

    public void endTurbo() {
        this.turboApplied = null;
    }
}
