package noobbot.strategy;

import com.google.inject.Inject;
import noobbot.adaptive.Adaptive;
import noobbot.data.Action;
import noobbot.data.CarControl;
import noobbot.data.Cars;

/**
 * Created by anita on 4/25/14.
 */

public class NoobStrategy implements Strategy {

    private final Cars allCarDetails;

    @Inject
    public NoobStrategy(Cars allCarDetails) {
        this.allCarDetails = allCarDetails;
    }

    @Override
    public CarControl takeAction(CarControl adaptiveControl) {

        // If a car is ahead of us and accelerating and we are in a curve, set throtle = 1.0 to follow the car
        if (allCarDetails.isCarInFrontSpeeding()) {

        }


        // If a faster car is ahead of us within 5 car lengths, use turbo and kick it off in a relatively straight stretch
        // If there is a slow car is in the same lane ahead of us and I have an option to switch, switch out of the track
        // If there is a car behind and they are using turbo, turn your turbo on
        // If there is a potential for a switch and the lane is crowded, do not switch

        // Huge penalty for crashing out of the track




        return adaptiveControl;
    }
}
